
import io
import nltk
import itertools
from operator import itemgetter
from rank import *

import os

#filters out everything except nouns and adjectives
def filter_for_tags(tagged, tags=['NN', 'JJ', 'NNP']):
    return [item for item in tagged if item[1] in tags]

def normalize(tagged):
    return [(item[0].replace('.', ''), item[1]) for item in tagged]


def getKeywords(text):
    #tokenize the text using nltk
	words = nltk.word_tokenize(text)

	#get position tags for each word 
	taggedWords = nltk.pos_tag(words)
    
    #only nouns and adjectives are considered for being included in keyWords
	taggedWords = filter_for_tags(taggedWords)
	taggedWords = normalize(taggedWords)

	uniqueWords = list(set([x[0] for x in taggedWords]))

	
	wordsList=[]
	
	#ignore small words
	for x in uniqueWords:
		if(len(x)>2):
			wordsList.append(x)
	
	graph = buildGraph(wordsList)

	#get ranks for the words
	ranks = getRanks(graph)

	#most important words in ascending order of importance
	ranks.sort(key=lambda x: (x[1] * -1, x[0]))

	#one third of the original words selected as keyWords
	aThird = len(wordsList) / 3
	ranks = ranks[0:aThird+1]
	
        
	return [x[0] for x in ranks]

