#2012B5A7848P-Saurabh Kumar
#PageRank Algorithm

import io
import nltk,re,pprint
import itertools
import math
from graph import graph


def lDistance(firstString, secondString):
    #Function to find the Levenshtein distance between two words http://rosettacode.org/wiki/Levenshtein_distance#Python
    if len(firstString) > len(secondString):
        firstString, secondString = secondString, firstString
    distances = range(len(firstString) + 1)
    for index2, char2 in enumerate(secondString):
        newDistances = [index2 + 1]
        for index1, char1 in enumerate(firstString):
            if char1 == char2:
                newDistances.append(distances[index1])
            else:
                newDistances.append(1 + min((distances[index1], distances[index1+1], newDistances[-1])))
        distances = newDistances
    return distances[-1]

#represents text as graph	
def buildGraph(nodes):
	gr=graph(nodes)
	#nodePairs contains all the combinations of nodes
	nodePairs = list(itertools.combinations(nodes, 2))

	#add edges to the graph (weighted by similarity between them)
	for pair in nodePairs:
		firstString = pair[0]
		secondString = pair[1]
		#lDistance is normalized to treat shorter and longer words alike
		gr.addEdge(firstString, secondString, (lDistance(firstString,secondString)/(math.log(len(firstString),2)+math.log(len(secondString),2))))
	return gr
	
#helper function for calculating rank	
def weightSum(nodes):
	i=0
	s=0;
	while(i<len(nodes)):
		s=s+nodes[i]
		i=i+1
	return s

#helper function for calculating rank	
def neighbourImportance(graph,nodes,ranks):
	i=0;
	importance=0.0
	while(i<len(nodes)):
		if(weightSum(graph.getNeighbours(i))>0):
			importance=importance+(ranks[i][1]*nodes[i]/weightSum(graph.getNeighbours(i)))
		i=i+1
		
	return importance


#function to rank nodes in decreasing order of importance    
def getRanks(graph,alpha=0.85):
	if len(graph.nodeContent) == 0:
		return {}
	else:
		#initially every sentence is given equal rank i.e. 1. It could be any number to start with
		temp=1.0
		
		nodeRanks = [[temp for i in range(2)] for j in range(len(graph.nodeContent))]
		tempRanks = [100.0] * len(graph.nodeContent)
		
		i=0
		
		while(i<len(tempRanks)):
			nodeRanks[i][0]=graph.getNode(i)
			i=i+1
		
		#flag to indicate convergence	
		hasConverged=0
		while(hasConverged==0):
			hasConverged=1
			i=0;
			while(i<len(tempRanks)):
				#calculates new rank for each sentence
				tempRanks[i]=(1-alpha)+alpha*neighbourImportance(graph,graph.getNeighbours(i),nodeRanks)
				if(math.fabs(tempRanks[i]-nodeRanks[i][1])>0.0001):
					hasConverged=0
				i=i+1	
					
			i=0
			while(i<len(tempRanks)):
				nodeRanks[i][1]=tempRanks[i]
				i=i+1
	return nodeRanks
